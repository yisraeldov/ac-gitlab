;;; ac-gitlab.el --- auto complete source for gitlab

;; Author: Yisrael Dov Lebow
;; Package-Requires: ((auto-complete "1.5.0"))


;;; Commentary:

;; auto-complete source for gitlab commits and markdown

;;; Code:
(require 'auto-complete)
(require 'cl-extra)
(ac-define-source gitlab-quick-actions
  '((candidates . ac-gitlab-quick-action-canidates)
    (prefix . "/\\(.*\\)")
    (symbol . "f")
    ))

(defvar ac-gitlab-directory (file-name-directory (or load-file-name buffer-file-name)))

(split-string "| `/tableflip <Comment>`     | Append the comment with `(╯°□°)╯︵ ┻━┻` | ✓ | ✓        |" "|" nil " +")
(defun ac-gitlab-load-quick-actions-from-md ()
  "Try to load the quick actios from the markdown documentation."
    (with-temp-buffer
      (insert-file-contents (concat ac-gitlab-directory "gitlab-quick_actions.md"))
      (cl-map
       'list
       (lambda (line) (split-string line "|" nil " +"))
       (seq-filter (lambda (line) (and (string-prefix-p "|" line) (not (string-prefix-p "|:" line)) ))
		   (split-string (buffer-string) "\n" t)))
      ))

(defun ac-gitlab-quick-action-canidates ()
  "Convert the list of lists to popup."
  (cl-map
   'list (lambda (row)
	   (popup-make-item (ac-gitlab--extract-quick-action (nth 1 row))
			    :summary (nth 1 row)
			    :document (nth 2 row)
			    ))
   (ac-gitlab-load-quick-actions-from-md)
   )
  )

(defun ac-gitlab--extract-quick-action (command)
  "Extract only the name of the quick action from the COMMAND."
  (nth 0 (split-string command " " t "[`/]*"))
  )

(defun ac-gitlab-set-up ()
  "Add gitlab sources."
  (interactive)
  (add-to-list 'ac-sources 'ac-source-gitlab-quick-actions))

(provide 'ac-gitlab)
;;; ac-gitlab.el ends here
