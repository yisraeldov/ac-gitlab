# Auto complete for gitlab

This is a basic source for auto-complete using gitlab 

Right now only *quick actions* are supported.

![screenshot](screenshot.png)


## Usage 

Clone this repo some where in your load path.

You can then enable it like this `using use-package`.

``` emacs-lisp
(use-package ac-gitlab
    :load-path "lib/ac-gitlab"
	:hook ((git-commit-mode . ac-gitlab-set-up)
	       (forge-post-mode . ac-gitlab-set-up)
	       (markdown-mode . ac-gitlab-set-up))
	:config
	(add-to-list 'ac-modes 'git-commit-mode)
	(add-to-list 'ac-modes 'forge-post-mode)
	(add-to-list 'ac-modes 'markdown-mode)
)

```

You should now get auto complete for quick actions after the `/` char.

